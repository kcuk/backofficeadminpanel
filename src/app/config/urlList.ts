export var URL_LIST = {
    "BASE_URL": {
        "API": "http://13.233.163.36/"
    },
    "LOGIN": "authentication/login/",
    "ADD_EMPLOYEE": "authentication/employee/",
    "EMPLOYEE": {
        "GET_EMPLOYEE": "authentication/employee/"        
    },
    "SOCIETY":{
        "ADD_SOCIETY":"authentication/socity/"
    },
    "RESISDENT":{
        "GET_RESISDENT":"authentication/get_residents/"
    }
}