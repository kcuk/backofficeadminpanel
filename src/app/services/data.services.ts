import { Injectable } from '@angular/core';
// import { Response, Headers, RequestOptions, Http, URLSearchParams } from "@angular/http";
// import { Observable, Subject } from "rxjs";
import { LocalStorageService } from 'ngx-webstorage';

// import 'rxjs/add/operator/catch';
// import  'rxjs/add/operator/map';
// import 'rxjs/add/operator/do';
import { catchError } from 'rxjs/operators/catchError';
import { map } from 'rxjs/operators/map';
import { Observable, Subject } from 'rxjs';
import 'rxjs/add/observable/throw';



import {
    HttpResponse,
    HttpRequest, HttpHandler, HttpClient, HttpHeaders,
    HttpEvent,
    HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { switchMap } from 'rxjs/operators';

// import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
@Injectable()
export class DataService {
    api: any;
    baseApiPath: string;
    indexData: any;
    autocompleteArray: any;
    userSession: any;

    // action role network mapping variables
    actionIdNameMappingObj: any;
    actionAccess: any;
    actionNetworkNameMappingObj: any;
    selectedNetworkAccess: any;
    viewModeFlag: any;

    dropdownContentObj: any;
    constructor(public httpClient: HttpClient, private localStorageService: LocalStorageService) {

    }

    getTimeZone(): any {
        // const regExp = /\(([^)]+)\)/;        
        // const matches: Array<any> = regExp.exec(new Date().toString());
        // if (matches.length > 0) {
        //     const timeZoneName: string = matches[0];
        //     return timeZoneName.substring(1, timeZoneName.length - 1);
        // } else {
        //     return null;
        // }
        return new Date().getTimezoneOffset();
    }

    getToken(): string {
        // const token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI0MjgxZjgxYTgxMDY0ZTkzOTNjY2I0YTEzMWFhNWQ3YSIsImV4cCI6MTUzNzM0MDAxNH0.H3Xgb_OztPd35ce7OC73BK3a0O7bfpmBeUNEOC5CQD66QYoConfbEz4KKCcvNo2HXtxaLuzOrgPc1jDxjh5Frw';
        // const token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiI5MTc5ODJiNjhlZDA0ODU5OThlMzM1MmE0ZjVhNmMyYyIsImV4cCI6MTUzNzM1NTM5MH0.nqpCAvlhv1WBruK4889SCvFAfgTiNautc-xmQaa-bfmXlIeKeEQQmROXbHcV5UBedvIc0lw7sJNQC1FAio6dLg';
        // return token;
        // return this.getCookie('token');
        if (this.localStorageService.retrieve('token')) {
            let tokenObj = this.localStorageService.retrieve('token');
            return tokenObj.token_type+" "+tokenObj.access_token;
        }
        else
            return null;
        // if (this.getCookie('token')) {
        //     return this.getCookie('token');
        // } else {
        //     return '';
        // }
    }
    errorList = [];
    setValidationErrors(errors) {
        this.errorList = errors;
    }
    getValidationErrors() {
        return this.errorList;
    }
    updateAutoCompleteArray(data: any) {
        this.autocompleteArray = data;
    }

    getSession(): string {
        let session;
        if (this.localStorageService.retrieve('user')) {
            const user = this.localStorageService.retrieve('user');
            session = user.session_id;
            return session;
        } else {
            return null;
        }
    }










    // {
    //     "data": {
    //         "access_token": "oJR7b1AEoaxNGSW9mm2GFz235MzeVR",
    //         "expires_in": 3600000,
    //         "token_type": "Bearer",
    //         "refresh_token": "SElnv0jAucxpkjdwluK7HilliZbvsK",
    //         "client_id": "KoOV9jmpYKt521clJVnprsLWtiAy52pg5HeY6TUa",
    //         "client_secret": "u3ELxnhtSq9P8JuPLwGubrSoCcxON01pXq7xrkcsGrhLt8qm6B69D8rGk0oRYWeN4cIUhXlKlpkA0djBxgoqG3xN8hcNkJK6P7XrUn4fY9bqxhlSYAilqakWfJD7jIFB",
    //         "name": "admin",
    //         "email": "maidsncar@gmail.com",
    //         "success": true
    //     }
    // }

    callRestful(type: string, url: string, options?: { params?: {}, body?: {} }) {
        const httpHeaders = {
            headers: new HttpHeaders()
                .set('Content-Type', 'application/json')

                //     // .set("Content-Type", "application/x-www-form-urlencoded")
                //     // .set("oauth", this.getToken()?this.getToken():"")
                //     // .set("session", this.getSession()?this.getSession():"")
                //     .set('zone', this.getTimeZone())
                .set('Authorization', this.getToken())
            // // .set('Access-Control-Allow-Origin','*')
            // // .set('Access-Control-Allow-Credentials' , 'true')
            // // .set('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE')
        };

        switch (type) {
            case 'GET':
                // return this.httpClient.get<any>(url, httpHeaders).pipe(map((res) => res), catchError(error => this.handleError(error)));
                return this.httpClient.get<any>(url, httpHeaders);
            case 'POST':
                // return this.httpClient.post<any>(url, options.body, httpHeaders).pipe(map((res) => res), catchError(error => this.handleError(error)));
                return this.httpClient.post<any>(url, options.body, httpHeaders);
            case 'PUT':
                // return this.httpClient.put<any>(url, options.body, httpHeaders).pipe(map((res) => res), catchError(error => this.handleError(error)));
                return this.httpClient.put<any>(url, options.body, httpHeaders);
            case 'DELETE':
                // return this.httpClient.delete<any>(url, httpHeaders).pipe(map((res) => res), catchError(error => this.handleError(error)));
                return this.httpClient.delete<any>(url, httpHeaders);
            default:
                return null;
        }
    }

    callRestfulWithFormData(method, url: string, formDataArray: Array<any>): any {
        const fd = new FormData();
        formDataArray.forEach((element) => {
            fd.append(element.key, element.value);
        });

        const xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        //   xhr.setRequestHeader('zone', this.getTimeZone());
        xhr.setRequestHeader('Authorization', this.getToken());
        xhr.send(fd);
        return xhr;
    }

    callRestfulWithAdditionalForm(method, url: string, fd?: FormData): any {
        const xhr = new XMLHttpRequest();
        xhr.open(method, url, true);
        xhr.setRequestHeader('zone', this.getTimeZone());
        xhr.setRequestHeader('access-token', this.getToken());
        fd ? xhr.send(fd) : xhr.send();
        //xhr.send(fd);
        return xhr;
    }

    private handleError(error: Response | any, isNotError?: boolean) {
        // this.spinner.hide();
        if (!isNotError && !error.ok) {
            if (parseInt(error.status) != 504) {
                if (error.status) {
                    this.showMessage('error', 'Application session timeout. Please refresh the page.');
                } else {
                    this.showMessage('error', 'Application session timeout. Please refresh the page.');
                }
            }
            // this.logError(error);
            if (parseInt(error.status) === 503) {
                this.showMessage('error', 'Application session timeout. Please refresh the page.');
            }
        }
        let errMsg: string;
        // TODO error message on screen
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        return Observable.throw(errMsg);
    }

    // ====================================================
    // ===== Function to show Error & Success Message =====
    // ====================================================
    showMessage(cssClass, msg) {
        let x = document.getElementById('alert-msz');
        let classType = '';
        x.innerHTML = msg;
        if (cssClass === 'error') {
            classType = 'show-error';
        }
        if (cssClass === 'success') {
            classType = 'success-msz';
        }
        x.className = classType;
        setTimeout(function () {
            x.className = x.className.replace(classType, '');
        }, 5000);
    }

    public getCookie(name: string) {
        return localStorage.getItem(name);
    }

    public deleteCookie(name: any) {
        localStorage.removeItem(name);
    }

    public setCookie(name: string, value: any) {
        localStorage.setItem(name, value);
    }

    public getSessionValue(name: string) {
        return sessionStorage.getItem(name);
    }

    public deleteSessionValue(name: any) {
        sessionStorage.removeItem(name);
    }

    public setSessionValue(name: string, value: any) {
        sessionStorage.setItem(name, value);
    }

    //     startSpinner() {
    //         this.spinner.show();
    //     }

    //     stopSpinner(delay?: any) {
    //         this.spinner.hide(delay = 0);
    //     }
}
