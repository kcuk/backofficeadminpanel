import { EditEmployeeComponent } from './employee/editEmployee/editEmployee.component';
import { AddEmployeeComponent } from './employee/addEmployee/addEmployee.component';
import { LoginComponent } from './login/login.component';
import { EditSocietyComponent } from './society/editSociety/editSociety.component';
import { AddSocietyComponent } from './society/addSociety/addSociety.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';
import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SocietyComponent } from './society/society.component';
import { ResidentComponent } from './resident/resident.component';
import { EmployeeComponent } from './employee/employee.component';

const routes: Routes = [
    {
        path: '', component: PagesComponent, children: [          
            {
                path: 'dashboard', component: DashboardComponent, children: [
                    { path: 'society', component: SocietyComponent },
                    { path: 'resident', component: ResidentComponent },
                    { path: 'employee', component: EmployeeComponent },
                    { path: 'society/add-society', component: AddSocietyComponent },
                    { path: 'society/edit-society/:id', component: AddSocietyComponent },
                    { path: 'employee/add', component: AddEmployeeComponent },
                    { path: 'employee/edit/:id', component: AddEmployeeComponent }
                ]
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: [

    ],
})
export class PagesRoutingModule { }