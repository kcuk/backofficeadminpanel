import { DataService } from './../../../services/data.services';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { URL_LIST } from '../../../config/urlList';

@Component({
  selector: 'app-addSociety',
  templateUrl: './addSociety.component.html',
  styleUrls: ['./addSociety.component.css']
})
export class AddSocietyComponent implements OnInit {

  public societyForm: FormGroup;

  isSocietyAdd: boolean = true;
  societyId: string = null;

  constructor(public activatedRoute: ActivatedRoute, public dataService: DataService, public formBuilder: FormBuilder) { }

  ngOnInit() {
    this.isSocietyAdd = true;
    this.societyForm = this.formBuilder.group(
      {
        "name": ['', [Validators.required]],
        "total_towers": ['', [Validators.required]],
        "state": ['', [Validators.required]],
        "city": ['', [Validators.required]],
        "area": ['', [Validators.required]],
        "pincode": ['', [Validators.required]]
      }
    );
    this.activatedRoute.params.subscribe((data) => {
      if (data.id) {
        this.isSocietyAdd = false;
        this.societyId = data.id;
        this.getSocietyById();
      }
    })
  }


  getSocietyById() {
    let url = URL_LIST.BASE_URL.API + URL_LIST.SOCIETY.ADD_SOCIETY + this.societyId + '/';
    this.dataService.callRestful("GET", url).subscribe((data) => {
      if (data.success) {
        let res = data.data;
        for (let key in this.societyForm.value) {
          this.societyForm.controls[key].setValue(res[key]);          
        }
        this.societyId=res.id;
      }
    })
  }

  addEditSociety() {
    let basUrl = URL_LIST.BASE_URL.API + URL_LIST.SOCIETY.ADD_SOCIETY;
    let url = this.isSocietyAdd ? basUrl : basUrl + this.societyId + '/';
    this.dataService.callRestful(this.isSocietyAdd ? "POST" : "PUT", url, { body: this.societyForm.value }).subscribe((data) => {
      if (data.success) {
        alert(data.message);
      }
      if (!data.success) {
        alert(data.error);
      }
    })
  }



}
