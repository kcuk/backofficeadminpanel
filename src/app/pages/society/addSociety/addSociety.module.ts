import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddSocietyComponent } from './addSociety.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class AddSocietyModule { }
