import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SocietyComponent } from './society.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class SocietyModule { }
