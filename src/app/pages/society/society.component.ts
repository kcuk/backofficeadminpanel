import { URL_LIST } from './../../config/urlList';
import { DataService } from './../../services/data.services';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-society',
  templateUrl: './society.component.html',
  styleUrls: ['./society.component.css']
})
export class SocietyComponent implements OnInit {
  searchBy = "name";
  searchText;
  constructor(public dataService: DataService) {

  }

  societyList = [];

  ngOnInit() {
    this.getSocietyList();
  }

  getSocietyList() {
    let url = URL_LIST.BASE_URL.API + URL_LIST.SOCIETY.ADD_SOCIETY;
    this.dataService.callRestful("GET", url).subscribe((data) => {
      if (data.success) {
        this.societyList = data.data;
      }
    })
  }

  searchSociety(event) {
    if(event.keyCode==13)
    {
    let url = URL_LIST.BASE_URL.API + URL_LIST.SOCIETY.ADD_SOCIETY + '/?search_by=' + this.searchBy + '&search=' + this.searchText;
    this.dataService.callRestful("GET", url).subscribe((data) => {
      if (data.success) {
        this.societyList = data.data;
      }
    });}
  }



}
