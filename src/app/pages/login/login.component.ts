import { URL_LIST } from './../../config/urlList';
import { DataService } from './../../services/data.services';
import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage'; 
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm: FormGroup;

  public isError:boolean=false;
  constructor(public localStorageService:LocalStorageService,public router: Router, public dataService: DataService, public formBuilder: FormBuilder) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group(
      {
        email: ['maidsncar@gmail.com', [Validators.required, Validators.email]],
        password: ['maidsncar', [Validators.required]]
      }
    )
  }


  getLogin() {
    this.isError=false;
    let obj = {
      "cipher_text": btoa(this.loginForm.controls['email'].value + ":" + this.loginForm.controls['password'].value)
    };
    this.dataService.callRestful("POST", URL_LIST.BASE_URL.API + URL_LIST.LOGIN, { body: obj }).subscribe((data) => {
      let res = data.data;
      if (res.success) {
         this.localStorageService.store('token',res); 
         this.router.navigateByUrl('/dashboard/society');
      }
      else{
        this.isError=true;
      }
    });
  }

}
