import { EditEmployeeComponent } from './employee/editEmployee/editEmployee.component';
import { AddEmployeeComponent } from './employee/addEmployee/addEmployee.component';
import { EditSocietyComponent } from './society/editSociety/editSociety.component';
import { AddSocietyComponent } from './society/addSociety/addSociety.component';
import { BrowserModule } from '@angular/platform-browser';
import { LoginComponent } from './login/login.component';

import { DashboardComponent } from './dashboard/dashboard.component';
import { SocietyComponent } from './society/society.component';
import { ResidentComponent } from './resident/resident.component';
import { EmployeeComponent } from './employee/employee.component';
import { PagesRoutingModule } from './pages.routing';
import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';


@NgModule({
    imports: [PagesRoutingModule,
        ReactiveFormsModule, FormsModule,
        BrowserModule
    ],
    exports: [],
    declarations: [PagesComponent, EmployeeComponent,
        ResidentComponent,
        SocietyComponent,
       // LoginComponent,
        DashboardComponent,
        AddSocietyComponent,
        EditSocietyComponent,
        AddEmployeeComponent,
        EditEmployeeComponent
    ],
    providers: [],
})
export class PagesModule { }
