import { DataService } from './../../services/data.services';
import { Component, OnInit } from '@angular/core';
import { URL_LIST } from '../../config/urlList';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeeComponent implements OnInit {

  employeeList: Array<any> = [];
  searchBy = "name";
  searchText;
  constructor(public dataService: DataService) {

  }

  ngOnInit() {
    this.getEmployeeList();
  }

  getEmployeeList() {
    let url = URL_LIST.BASE_URL.API + URL_LIST.EMPLOYEE.GET_EMPLOYEE;
    this.dataService.callRestful("GET", url).subscribe((data) => {
      let response = data;
      if (data.success) {
        this.employeeList = data.data;
      }
    })
  }

  searchEmployee(event) {
    if (event.keyCode == 13) {
      let url = URL_LIST.BASE_URL.API + URL_LIST.EMPLOYEE.GET_EMPLOYEE + '/?search_by=' + this.searchBy + '&search=' + this.searchText;
      this.dataService.callRestful("GET", url).subscribe((data) => {
        if (data.success) {
          this.employeeList = data.data;
        }
      });
    }
  }



}
