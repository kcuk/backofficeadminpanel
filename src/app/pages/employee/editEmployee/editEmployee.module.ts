import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EditEmployeeComponent } from './editEmployee.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class EditEmployeeModule { }
