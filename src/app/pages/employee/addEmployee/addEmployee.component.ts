import { URL_LIST } from './../../../config/urlList';
import { DataService } from './../../../services/data.services';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-addEmployee',
  templateUrl: './addEmployee.component.html',
  styleUrls: ['./addEmployee.component.css']
})
export class AddEmployeeComponent implements OnInit {
  isEmployeeEdit: boolean = false;
  fileToUpload: File = null;
  employeeId = null;
  imagePath=null;
  public services = [
    { name: 'Select Service', value: '' },
    { name: 'Maids', value: 'Maids' },
    { name: 'Cook', value: 'Cook' },
    { name: 'Child Care', value: 'Child Care' },
    { name: 'Elder Care', value: 'Elder Care' },
    { name: 'Car Cleaner', value: 'Car Cleaner' },
    { name: 'Driver', value: 'Driver' }
  ]

  employeeFormGroup: FormGroup;
  job_location: FormArray;

  constructor(public activatedRoute: ActivatedRoute, public formBuilder: FormBuilder, public dataService: DataService) {
    this.employeeFormGroup = this.formBuilder.group(
      {
        "name": ['', [Validators.required]],
        "adhaar_no": ['', [Validators.required]],
        "mobile": ['', [Validators.required]],
        "family_member_no": ['', [Validators.required]],
        "emergency_no": ['', [Validators.required]],
        "land_lord_no": ['', [Validators.required]],
        "land_lord_address": ['', [Validators.required]],
        "current_address": ['', [Validators.required]],
        "permanent_address": ['', [Validators.required]],
        "age": ['', [Validators.required]],
        "police_verify": ['', [Validators.required]],
        "service_name": ['', [Validators.required]],
        "timing": ['', [Validators.required]],
        //  "job_location": [{ "socity_id": 2, "tower_no": "B", "flat_no": 325 }],
        "job_location": this.formBuilder.array([this.createItem()])
      }
    );
   // this.addItem();

  }

  ngOnInit() {
    this.isEmployeeEdit = false;
    this.employeeId = null;
    this.activatedRoute.params.subscribe((data) => {
      if (data.id) {
        this.isEmployeeEdit = true;
        this.employeeId = data.id;
        this.getEmployeeById();
      }
    })
  }

  getEmployeeById() {
    let url = URL_LIST.BASE_URL.API + URL_LIST.EMPLOYEE.GET_EMPLOYEE + this.employeeId;
    this.dataService.callRestful("GET", url).subscribe((data) => {
      let response=data;
      if(data.success)
      {
        let emp=data.data;
        this.employeeFormGroup.controls.name.setValue(emp.name);
        this.employeeFormGroup.controls.adhaar_no.setValue(emp.adhaar_no);
        this.employeeFormGroup.controls.age.setValue(emp.age);
        this.employeeFormGroup.controls.mobile.setValue(emp.mobile);
        this.employeeFormGroup.controls.police_verify.setValue(emp.police_verify);
        this.employeeFormGroup.controls.current_address.setValue(emp.current_address);
        this.employeeFormGroup.controls.family_member_no.setValue(emp.family_member_no);
        this.employeeFormGroup.controls.land_lord_address.setValue(emp.land_lord_address);
        this.employeeFormGroup.controls.land_lord_no.setValue(emp.land_lord_no);
        this.employeeFormGroup.controls.service_name.setValue(emp.service_name);
        this.employeeFormGroup.controls.emergency_no.setValue(emp.emergency_no);
        this.employeeFormGroup.controls.permanent_address.setValue(emp.permanent_address);
        this.imagePath=URL_LIST.BASE_URL.API+emp.profile_pic;
        this.changeJobType();
      }
    });
  }
 


  // handleFileInput(files) {
  //   console.log(files.files);
  //   this.fileToUpload = files.files.item(0);
  //  // console.log(this.fileToUpload);
  // }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    var reader = new FileReader();
    reader.onload = this.imageIsLoaded;
    reader.readAsDataURL(files[0]);
    console.log(this.fileToUpload);
  }



  addEditEmployee() {
    let values = this.employeeFormGroup.value;
    let formdata = [];
    for (let val in values) {
      formdata.push({ key: val, value: values[val] });
    }
    formdata.push({ key: 'profile_pic', value: this.fileToUpload });
  

    console.log(formdata);
    let xhr = new XMLHttpRequest();
    let baseUrl=URL_LIST.BASE_URL.API + URL_LIST.ADD_EMPLOYEE;
    let url = this.isEmployeeEdit?baseUrl+this.employeeId+'/':baseUrl;
    xhr = this.dataService.callRestfulWithFormData(this.isEmployeeEdit?"PUT":"POST", url, formdata);
    xhr.onreadystatechange = (function (request: XMLHttpRequest, event: Event) {
      if (request.readyState == 4 && request.status == 200) {
        let res: any = request.responseText;
        res = JSON.parse(res);
        if (res.status == "success") {

        }
        else {

        }


      }
    }).bind(this, xhr);



  }


  id: HTMLImageElement
  imageIsLoaded(e) {   
    document.getElementById('emp_img').setAttribute('src', e.target.result);
    //  document.     
  };

  addItem(no) {
    this.job_location = this.employeeFormGroup.get('job_location') as FormArray;
    for (let i = 0; i < no; i++) {
      this.job_location.push(this.createItem());
    }
  }

  createItem(): FormGroup {
    return this.formBuilder.group({
      socity_id: '',
      tower_no: '',
      flat_no: ''
    });
  }

  changeJobType()
  {
    this.employeeFormGroup.controls['timing'].setValue([]);
    if(this.employeeFormGroup.controls['timing'].value=="24" || this.employeeFormGroup.controls['timing'].value=="12")
    {
      this.addItem(1);  
    }
    if(this.employeeFormGroup.controls['timing'].value=='part')
    {
      this.addItem(10);  
    }
  }



}
