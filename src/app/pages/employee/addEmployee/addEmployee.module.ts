import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddEmployeeComponent } from './addEmployee.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class AddEmployeeModule {

}
