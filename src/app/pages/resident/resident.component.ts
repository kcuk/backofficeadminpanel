import { DataService } from './../../services/data.services';
import { Component, OnInit } from '@angular/core';
import { URL_LIST } from '../../config/urlList';

@Component({
  selector: 'app-resident',
  templateUrl: './resident.component.html',
  styleUrls: ['./resident.component.css']
})
export class ResidentComponent implements OnInit {

  public resisdentList:Array<any>=[];
  constructor(public dataService: DataService) { }

  ngOnInit() {
    this.getResisdent();
  }

  getResisdent() {
    let url = URL_LIST.BASE_URL.API + URL_LIST.RESISDENT.GET_RESISDENT;
    this.dataService.callRestful('GET', url).subscribe((data) => {
      let res=data;
      if(res.success)
      {
        this.resisdentList=res.data;
        
      }
    })
  }

}
