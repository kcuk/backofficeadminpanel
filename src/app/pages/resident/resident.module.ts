import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ResidentComponent } from './resident.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: []
})
export class ResidentModule { }
