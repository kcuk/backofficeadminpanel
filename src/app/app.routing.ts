import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { LoginComponent } from './pages/login/login.component';

const routes: Routes = [

    { path: '', component: AppComponent,children:[
       { path:'',component:LoginComponent},
       { path:'dashboard',component:PagesComponent}
      // { path:'',component: }
    ] }
  
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)],
    exports: [RouterModule],
    declarations: [],
})
export class AppRoutiongModule { }